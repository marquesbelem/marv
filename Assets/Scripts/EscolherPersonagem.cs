﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscolherPersonagem : MonoBehaviour
{
    public GameObject player1;
    public GameObject player2;
    public GameObject player3;

    public GameObject painelEscolha;
    public GameObject controle1;
    public GameObject controle2;
    public GameObject controle3;

    public void TrocaPersonagem(int _player)
    {
       if(_player == 1)
        {
            player1.SetActive(true);
            controle1.SetActive(true);
        }

        if (_player == 2)
        {
            player2.SetActive(true);
            controle2.SetActive(true);
        }

        if (_player == 3)
        {
            player3.SetActive(true);
            controle3.SetActive(true);
        }


        painelEscolha.SetActive(false);
        Controller._intance.Iniciar();
    }


}
