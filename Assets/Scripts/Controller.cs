﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public static Controller _intance = null;
    

    [SerializeField]
    private CameraFollow camFollow;
    void Awake()
    {
        if (_intance == null)
            _intance = this;

    }

    public void Iniciar()
    {
        camFollow.enabled = true;
    }
}
