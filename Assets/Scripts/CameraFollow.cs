﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public float velocidade = 0.15f;
    [SerializeField]
    private Transform target;

    public bool maxMin;
    public float xMin;
    public float xMax;
    public float yMin;
    public float yMax;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }
    
    void Update()
    {
        if (target)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, velocidade)  + new Vector3(0,0,transform.position.z);

            if (maxMin)
            {
                transform.position = new Vector3(Mathf.Clamp(target.position.x, xMin, xMax), 
                    Mathf.Clamp(target.position.y, yMin, yMax), transform.position.z);
            }
        }
    }
}
