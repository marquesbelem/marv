﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    /*public float velocidade = 300;
    private Rigidbody2D rb;

    private bool andar_direita;
    private bool andar_esquerda;
    private bool pular;

    private Animator animator;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            Pular(true);
        if (Input.GetKeyUp(KeyCode.Space))
            Pular(false);

        if (Input.GetKeyDown(KeyCode.D))
            AndarDireita(true);
        if (Input.GetKeyUp(KeyCode.D))
            AndarDireita(false);

        if (Input.GetKeyDown(KeyCode.A))
            AndarEsquerda(true);
        if (Input.GetKeyUp(KeyCode.A))
            AndarEsquerda(false);

        if (andar_direita)
            RunPlayer(10);

        if(andar_esquerda)
            RunPlayer(-10);

        if (pular)
            JumpPlayer(50);

        //Sair da telas
        Vector2 screen_position = Camera.main.WorldToScreenPoint(transform.position);
        
        if (screen_position.x < 0)
        {
            Debug.Log("saiu");
        }
        
        animator.SetFloat("andar", rb.velocity.magnitude);

    }

    void RunPlayer(float aux)
    {
        rb.AddForce(new Vector2((aux * velocidade * Time.deltaTime), 0));
    }
    
    public void AndarDireita(bool acao)
    {
        andar_direita = acao;

        if (andar_direita)
        {
            this.GetComponent<SpriteRenderer>().flipX = true;
            animator.ResetTrigger("parado");
            animator.ResetTrigger("pulo");
           // animator.SetTrigger("andar");
        }
        else
        {
            //animator.ResetTrigger("andar");
            animator.SetTrigger("parado");
        }
    }

    public void AndarEsquerda(bool acao)
    {
        andar_esquerda = acao;

        if (andar_esquerda)
        {
            this.GetComponent<SpriteRenderer>().flipX = false;
            animator.ResetTrigger("parado");
            animator.ResetTrigger("pulo");
           // animator.SetTrigger("andar");
        }
        else
        { 
            //animator.ResetTrigger("andar");
            animator.SetTrigger("parado");
        }
    }

    void JumpPlayer(float aux)
    {
        rb.AddForce(new Vector2(0, (aux * velocidade * Time.deltaTime)));
    }

    public void Pular(bool acao)
    {
        pular = acao;

        if (pular){
            animator.ResetTrigger("parado");
           // animator.ResetTrigger("andar");
            animator.SetTrigger("pulo");
        } else{
            animator.ResetTrigger("pulo");
            animator.SetTrigger("parado");
        }
    }*/

    public float speed;
    public float jumpForce;

    private float moveInputX;
    private float moveInputY;
    private Rigidbody2D rb;
    private bool moveButtonUI;

    private Animator animator;
    private bool jump;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {

        if (!moveButtonUI)
        {
            moveInputX = Input.GetAxis("Horizontal");

            if (moveInputX > 0)
                Flip(true);
            else if (moveInputX < 0)
                Flip(false);

            rb.velocity = new Vector2(moveInputX * speed, rb.velocity.y);

            if (Input.GetAxis("Jump") > 0)
                Jump();
        }
        else
        {
            if (moveInputX == 1)
                rb.velocity = new Vector2(Mathf.Lerp(0.6f * speed, moveInputX * speed, Time.deltaTime), rb.velocity.y);
            else if (moveInputX == -1)
                rb.velocity = new Vector2(Mathf.Lerp(-0.6f * speed, moveInputX * speed, Time.deltaTime), rb.velocity.y);
        }

        if (!jump)
            animator.SetFloat("andar", rb.velocity.magnitude);
    }

    public void Movement(int _dir)
    {
        if (_dir == 1)
            Flip(true);
        else if (_dir == -1)
            Flip(false);

        if (_dir == -1 || _dir == 1)
            moveButtonUI = true;
        else
            moveButtonUI = false;

        moveInputX = _dir;
    }

    void Flip(bool _flip)
    {
        this.GetComponent<SpriteRenderer>().flipX = _flip;
    }


    public void Jump()
    {
        rb.velocity = Vector2.up * jumpForce;

        jump = true;
        animator.SetBool("pulo",true);
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.CompareTag("Ground"))
        {
            jump = false;
            animator.SetBool("pulo",false);
        }
    }
}
